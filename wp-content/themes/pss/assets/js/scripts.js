const initSlider = () => {
  const swiper = new Swiper('.swiper', {
    // Optional parameters
    slidesPerView: 'auto',
    spaceBetween: 128,  
  
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  
    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar'
    },
  });
}

const handleFormLabel = () => {
  const inputs = document.querySelectorAll('.ps-form__control');
  inputs.forEach(input => {
    input.addEventListener('focus', hideLabel, false);
    input.addEventListener('focusout', setLabelPosition, false);
  })
}

const hideLabel = (evt) => {
  const target = evt.currentTarget;
  const targetParent = target.closest('.ps-form__group');
  targetParent.classList.add('is-focused');
}

const setLabelPosition = (evt) => {
  const target = evt.currentTarget;
  const targetParent = target.closest('.ps-form__group');
  if (target.value.length > 0) {
    targetParent.classList.remove('is-focused');
    targetParent.classList.add('is-filled');
  } else {
    targetParent.classList.remove('is-focused');
    targetParent.classList.remove('is-filled');
  }
}

const handleFormSent = () => {
  const form = document.querySelector('.wpcf7');
  form.addEventListener('wpcf7mailsent', function(event) {
    const formGroups = form.querySelectorAll('.ps-form__group');
    formGroups.forEach(function(group) {
      group.classList.remove('is-filled', 'is-focused');
    })
  }, false);
}

const handleScrollTo = () => {
  const scrollTrigger = document.querySelector('[data-scroll-to]');
  if (typeof(scrollTrigger) != 'undefined' && scrollTrigger != null) {
    const nextBlock = document.querySelector('.ps-section');
    const nextBlockTop = nextBlock.getBoundingClientRect().top;
    
    scrollTrigger.addEventListener('click', function() {
      window.scrollTo({
        top: nextBlockTop - 80,
        behavior: "smooth"
      });
    })
  }
}

const initMobileMenu = () => {
  const burger = document.querySelector('[data-burger-btn]');
  const header = document.querySelector('[data-header]');
  burger.addEventListener('click', function() {
    header.classList.toggle('is-open');
  });
}

window.onload = function() {
  initSlider();
  handleFormLabel();
  handleScrollTo();
  initMobileMenu();
  handleFormSent();
};