<?php get_header(); ?>

<main>
  <?php get_template_part('partials/front-page/_hero'); ?>
  <?php get_template_part('partials/front-page/_about'); ?>
  <?php get_template_part('partials/front-page/_education'); ?>
  <?php get_template_part('partials/front-page/_experience'); ?>
  <?php get_template_part('partials/front-page/_blog'); ?>
  <?php get_template_part('partials/front-page/_readings'); ?>
  <?php get_template_part('partials/front-page/_contact'); ?>
</main>

<?php get_footer(); ?>
