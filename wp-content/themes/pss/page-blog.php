<?php 

/* Template Name: Blog Page */ 

get_header(); ?>

<main class="ps-page">
  <?php get_template_part('partials/blog-page/_blog'); ?>
</main>

<?php get_footer(); ?>