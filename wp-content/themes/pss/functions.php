<?php 
include_once dirname(__FILE__) . '/inc/acf.php';
include_once dirname(__FILE__) . '/inc/post-types.php';
include_once dirname(__FILE__) . '/inc/actions.php';
include_once dirname(__FILE__) . '/inc/filters.php';
include_once dirname(__FILE__) . '/inc/scripts.php';
include_once dirname(__FILE__) . '/inc/styles.php';
include_once dirname(__FILE__) . '/inc/optimize.php';

/* ---------------------/
  Hide Admin Bar
/--------------------- */
add_filter('show_admin_bar', '__return_false');


/* ---------------------/
  Register page categories and tags
/--------------------- */
function page_cat_tags() {
  register_taxonomy_for_object_type('post_tag', 'page');
}
add_action( 'init', 'page_cat_tags' );

/* ---------------------/
  Register Navigation
/--------------------- */
register_nav_menus(array(
  'primary' => __('Primary Menu'),
  'footer' => __('Footer Menu'),
));

/* ---------------------/
  Allow svg
/--------------------- */
function add_file_types_to_uploads($file_types){
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );

  return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');


/* ---------------------/
  Contact form 7
/--------------------- */
add_filter( 'wpcf7_autop_or_not', '__return_false' );
// CF tel regex
function custom_filter_wpcf7_is_tel( $result, $tel ) {
  $pattern = '/^[+]38 '
		. '(?:\([0-9]{3}+\)|[0-9]{3}+) '
		. '\d{3}-\d{2}-\d{2}' 
		. '$/i';

  $result = preg_match( $pattern , trim($tel));
  return $result;
}

add_filter( 'wpcf7_is_tel', 'custom_filter_wpcf7_is_tel', 10, 2 );


/* ---------------------/
  Remove uncategorized
/--------------------- */
add_action('admin_menu','remove_default_post_type');
function remove_default_post_type() {
  remove_menu_page('edit.php');
}


//Page Slug Body Class
function add_slug_body_class( $classes ) {
  global $post;
  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// disable generated image sizes
function shapeSpace_disable_image_sizes($sizes) {
	
	unset($sizes['medium_large']); // disable medium-large size
	unset($sizes['1536x1536']);    // disable 2x medium-large size
	unset($sizes['2048x2048']);    // disable 2x large size
	
	return $sizes;
}
add_action('intermediate_image_sizes_advanced', 'shapeSpace_disable_image_sizes');

// disable scaled image size
add_filter('big_image_size_threshold', '__return_false');

// disable other image sizes
function shapeSpace_disable_other_image_sizes() {
	remove_image_size('post-thumbnail'); // disable images added via set_post_thumbnail_size() 
	remove_image_size('another-size');   // disable any other added image sizes
}
add_action('init', 'shapeSpace_disable_other_image_sizes');

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


add_theme_support('post-thumbnails');


function mine_add_custom_types( $query ) {
  if (!$query->is_main_query()) {
    return;
  }
  // the rest of your code
}
add_filter( 'pre_get_posts', 'mine_add_custom_types' );

?>