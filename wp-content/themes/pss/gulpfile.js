'use strict';

const gulp = require('gulp');
const { watch, series, parallel } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const browserSync = require('browser-sync').create();

function buildStyles() {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
			cascade: false
		}))
    .pipe(gulp.dest('./assets/css'));
};

function minifyStyles() {
  return gulp.src('./assets/css/*.css')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./assets/minified/css'))
    .pipe(browserSync.stream());
}

function minifyJs() {
  return pipeline(
    gulp.src('./assets/js/*.js'),
    uglify(),
    rename({
      suffix: '.min'
    }),
    gulp.dest('./assets/minified/js')
  );
}



exports.default = function () {
  browserSync.init({
    proxy: 'http://localhost:8888/pss/',
    port: 3030
  });

  watch('./assets/sass/**/*', series(buildStyles, minifyStyles));
  watch('./assets/js/*', minifyJs).on('change', browserSync.reload);
  watch('./*.php').on('change', browserSync.reload);
  watch('./partials/**/*').on('change', browserSync.reload);
};