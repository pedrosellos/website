<?php 

/* Template Name: Readings Page */ 

get_header(); ?>

<main class="ps-page">
  <?php get_template_part('partials/readings-page/_readings-list'); ?>
</main>

<?php get_footer(); ?>