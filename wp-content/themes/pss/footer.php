<footer class="ps-footer">
  <div class="ps-container">
    <div class="ps-footer__inner">
      <div class="ps-footer__text">Copyright 2021 - <?php echo date('Y'); ?> &copy; Pedro Sigaud Sellos</div>
    </div>
  </div>
</footer>
<?php wp_footer() ?>
</body>