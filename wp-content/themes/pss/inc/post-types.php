<?php
function cptui_register_my_cpts() {
  $labels = array(
		"name" => __("Articles", "pss"),
		"singular_name" => __("Article", "pss"),
		"menu_name" => __("Articles", "pss"),
		"all_items" => __("All Articles ", "pss"),
		"add_new" => __("Add Article", "pss"),
		"add_new_item" => __("Add new Article", "pss"),
		"edit_item" => __("Edit Article", "pss"),
		"new_item" => __("New Article", "pss"),
		"view_item" => __("View Article", "pss"),
		"view_items" => __("View Articles", "pss"),
		"search_items" => __("Search Articles", "pss"),
		"not_found" => __("No Articles  found", "pss"),
		"not_found_in_trash" => __("No Articles found in trash", "pss"),
	);

	$args = array(
		"label" => __("Articles", "pss"),
		"labels" => $labels,
		'public' => true,
    'has_archive' => true,
    "menu_position" => 20,
		"supports" => array("title", 'thumbnail', 'page-attributes', 'editor'),
    'menu_icon' => 'dashicons-welcome-write-blog',
    'show_in_rest' => true,
	);

  register_post_type("articles", $args);
  

  $labels = array(
    "name" => __("Readings", "pss"),
    "singular_name" => __("Reading", "pss"),
    "menu_name" => __("Readings", "pss"),
    "all_items" => __("All Readings", "pss"),
    "add_new" => __("Add Readings", "pss"),
    "add_new_item" => __("Add new Reading", "pss"),
    "edit_item" => __("Edit Readings", "pss"),
    "new_item" => __("New Reading", "pss"),
    "view_item" => __("View Reading", "pss"),
    "view_items" => __("View Readings", "pss"),
    "search_items" => __("Search Readings", "pss"),
    "not_found" => __("No Readings found", "pss"),
    "not_found_in_trash" => __("No Readings found in trash", "pss"),
  );

  $args = array(
    "label" => __("Readings", "pss"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => false,
    "show_ui" => true,
    "show_in_rest" => false,
    "rest_base" => "",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => false,
    "query_var" => true,
    "menu_position" => 20,
    "supports" => array('title', 'page-attributes', 'editor'),
    'menu_icon' => 'dashicons-megaphone',
    'show_in_rest' => true,
    'supports' => array('title', 'thumbnail')
  );

  register_post_type("readings", $args);
}
add_action('init', 'cptui_register_my_cpts', 0);


/* ---------------------/
  Create page for header/footer settings
/--------------------- */
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page('General settings');
}