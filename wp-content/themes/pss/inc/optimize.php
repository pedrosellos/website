<?php
// Async load
function ikreativ_async_scripts($url) {
  if (strpos($url, '#deferload') === false)
    return $url;
  else if (is_admin())
    return str_replace('#deferload', '', $url);
  else
	  return str_replace('#deferload', '', $url)."' defer='defer"; 
  }
add_filter( 'clean_url', 'ikreativ_async_scripts', 11, 1 );

// Remove attr type
add_action( 'template_redirect', function() {
  ob_start(function($buffer) {
    $buffer = str_replace(array('type="text/javascript"', "type='text/javascript'" ), '', $buffer);        
    return $buffer;
  });
});

// add_filter('use_block_editor_for_post_type', '__return_false', 10);

// Remove wp-embed.min.js
function deregister_wp_embed() {
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'deregister_wp_embed' );

// Disable gutenberg & dequeue scripts/styles
add_action( 'wp_print_styles', 'deregister_gutenberg_styles', 100 );
function deregister_gutenberg_styles() {
  wp_dequeue_style('wp-block-library'); // Wordpress core
  wp_dequeue_style('wp-block-library-theme'); // Wordpress core
	wp_deregister_style( 'wp-block-library' );
  wp_dequeue_style('wc-block-style'); // WooCommerce
  wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
}

// Remove emoji styles
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


// Remove contact form 7 styles
add_filter( 'wpcf7_load_css', '__return_false' );

define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
