<?php
function reg_jq() {
  wp_deregister_script( 'jquery' );
}
add_action('wp_enqueue_scripts', 'reg_jq');

function load_my_scripts() {
  
  wp_register_script( 
    'main', 
    get_template_directory_uri() . '/assets/minified/js/scripts.min.js#deferload', 
    false,
    null,
    true
  );
  wp_register_script( 
    'swiper', 
    'https://unpkg.com/swiper@7/swiper-bundle.min.js#deferload', 
    false,
    null,
    true
  );
  wp_enqueue_script('swiper');
  wp_enqueue_script('main');
}
add_action('wp_enqueue_scripts', 'load_my_scripts');

add_action('acf/input/admin_enqueue_scripts', function() {
  wp_enqueue_style( 'horizontal-acf', '/wp-content/themes/pss/inc/acf-admin.css', [], null, '' );
});
