<?php
function get_cached_posts( $args, $time = 3600,$category_name = 'default' ) {
  $post_list_name = 'get_posts_' . md5($category_name);

  if ( false === ( $post_list = wp_cache_get( $post_list_name ) ) ) {
    $post_list = get_posts( $args );

    wp_cache_add( $post_list_name, $post_list,'', $time );
  }

  return $post_list; 
}



function misha_my_load_more_scripts() {
	wp_register_script(
    'JQ',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js#deferload',
    false,
    null,
    true
  );
  
  wp_register_script( 
    'my_loadmore', 
    get_stylesheet_directory_uri() . '/assets/minified/js/loadmore.min.js#deferload', 
    false,
    null,
    true
  );
 
	$args = array(
    'post_type' => 'articles',
    'posts_per_page' => 9,
    'orderby'	=> 'post_date',
    'order'   => 'DESC'
  );
  $query = new WP_Query($args); 
  
	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
		'posts' => json_encode( $query->query_vars ),
		'current_page' => $query->get_query_var( 'paged' ) ? $query->get_query_var('paged') : 1,
		'max_page' => $query->max_num_pages
	) );
 
  if (is_page_template('page-blog.php')):
    wp_enqueue_script('JQ');
    wp_enqueue_script('my_loadmore');
  endif;
}
 
add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );


function misha_loadmore_ajax_handler(){
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1;
	$args['post_status'] = 'publish';
 
	query_posts( $args );
 
	if( have_posts() ) :
 
		while( have_posts() ): the_post();
 
			get_template_part('partials/blog-page/_blog-thumb');
 
		endwhile;
 
	endif;
	die;
}
 
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); 