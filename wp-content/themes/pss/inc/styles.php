<?php
function theme_resources() {
  wp_enqueue_style('styles',  get_template_directory_uri() . '/assets/minified/css/styles.min.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_resources' );