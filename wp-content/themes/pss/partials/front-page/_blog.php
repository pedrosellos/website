<?php 
$blog_items = get_cached_posts(array(
  'post_type'    => 'articles',
  'posts_per_page' => 3,
  'post_status'  => 'publish'
), 3600, 'articles');
if ($blog_items): 
  $blogId = get_option('options_blog_page'); ?>
  <section class="ps-section">
    <div class="ps-container">
      <div class="ps-row ps-gutters">
        <div class="ps-col-md-4">
          <h2 class="is-t-uppercase"><?php echo get_the_title($blogId); ?></h2>
        </div>
      </div>
      <div class="ps-section__row">
        <div class="ps-article__list">
          <div class="ps-row ps-gutters">
            <?php foreach ($blog_items as $post): 
              get_template_part('partials/blog-page/_blog-thumb'); 
            endforeach; 
            wp_reset_postdata(); ?>
          </div>
        </div>
      </div>
      <div class="ps-btn__wrap_center">
        <a href="<?php echo esc_url(get_page_link($blogId)); ?>" class="ps-btn"><?php echo get_option('options_read_more_button'); ?></a>
      </div>
    </div>
  </section>
<?php endif; ?>