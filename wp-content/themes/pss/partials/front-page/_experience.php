<?php
$experienceList = get_post_meta($post->ID, 'experience_list', true); 
if ($experienceList): ?>
  <section class="ps-section ps-section_grey">
    <div class="ps-container">
      <div class="ps-row ps-gutters">
        <div class="ps-col-md-4">
          <h2 class="is-t-uppercase"><?php echo get_post_meta($post->ID, 'experience_title', true); ?></h2>
        </div>
      </div>
      <div class="ps-section__row">
        <div class="swiper ps-slider">
          <div class="swiper-wrapper">
              <?php
              for ($i = 0; $i < $experienceList; $i++): ?>
                <div class="swiper-slide ps-slider__item">
                  <ul class="ps-hierarchy__list">
                    <li class="ps-hierarchy__list--item h5"><?php echo get_post_meta($post->ID, 'experience_list_'.$i.'_position', true); ?></li>
                    <li class="ps-hierarchy__list--item h5 is-fw-medium"><?php echo get_post_meta($post->ID, 'experience_list_'.$i.'_place', true); ?></li>
                    <li class="ps-hierarchy__list--item ps-text_md is-color-t-secondary"><?php echo get_post_meta($post->ID, 'experience_list_'.$i.'_time', true); ?></li>
                    <li class="ps-hierarchy__list--item"><?php echo get_post_meta($post->ID, 'experience_list_'.$i.'_description', true); ?></li>
                  </ul>
                </div>
              <?php endfor; ?>
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-scrollbar"></div>
            <div class="swiper-button-next"></div>
            <!-- If we need scrollbar -->
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>