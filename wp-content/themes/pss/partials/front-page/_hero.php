<?php
$heroTitle = get_post_meta($post->ID, 'hero_title', true);
if ($heroTitle) : 
  $heroImage = get_post_meta($post->ID, 'hero_background_image', true); ?>
  <section class="ps-hero" <?php if($heroImage): ?>style="background-image: url('<?php echo wp_get_attachment_image_url($heroImage, 'full'); ?>'); " <?php endif; ?>>
    <div class="ps-container ps-hero__content">
      <div class="ps-row ps-gutters">
        <div class="ps-col-md-5 ps-col-lg-6">
          <h1 class="is-t-uppercase"><?php echo $heroTitle; ?></h1>
          <?php if ($heroBtn = get_post_meta($post->ID, 'hero_contact_btn', true)): ?>
            <a href="<?php echo $heroBtn['url']; ?>" class="ps-btn ps-hero__btn" target="_blank"><?php echo $heroBtn['title']; ?></a>
          <?php endif; ?>
        </div>
        <?php 
        if (get_post_meta($post->ID, 'hero_short_story_title', true)): ?>
          <div class="ps-col-lg-4 ps-offset-lg-2 ps-hero__copy">
            <h2 class="h5"><?php echo get_post_meta($post->ID, 'hero_short_story_title', true); ?></h2>
            <p><?php echo get_post_meta($post->ID, 'hero_short_story_description', true); ?></p>
            <div class="ps-scroll__wrap">
              <div class="ps-scroll ps-btn" data-scroll-to><?php echo get_option('options_scroll_button'); ?></div>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>