<?php
$aboutTitle = get_post_meta($post->ID, 'about_title', true);
if ($aboutTitle): ?>
  <section class="ps-section">
    <div class="ps-container">
      <div class="ps-row ps-gutters">
        <div class="ps-col-lg-4">
          <h2 class="is-t-uppercase"><?php echo $aboutTitle; ?></h2>
        </div>
        <div class="ps-col-lg-7 ps-offset-lg-1">
          <?php $digits = get_post_meta($post->ID, 'about_numbers', true); 
          if ($digits): ?>
            <div class="ps-digit__row">
              <?php
              for ($i = 0; $i < $digits; $i++): ?>
                <div class="ps-digit">
                  <div class="ps-digit__numb"><?php echo get_post_meta($post->ID, 'about_numbers_'.$i.'_digit', true); ?></div>
                  <div class="ps-digit__desc"><?php echo get_post_meta($post->ID, 'about_numbers_'.$i.'_description', true); ?></div>
                </div>
              <?php endfor; ?>
            </div>
          <?php endif; 
          if ($aboutContent = get_post_meta($post->ID, 'about_history', true)): ?>
            <p><?php echo $aboutContent; ?></p>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>