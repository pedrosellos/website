<section class="ps-section">
  <div class="ps-container">
    <div class="ps-row ps-gutters">
      <div class="ps-col-md-4">
        <h2 class="is-t-uppercase"><?php echo get_post_meta($post->ID, 'contact_home_title', true); ?></h2>
      </div>
    </div>
    <div class="ps-section__row ps-contacts">
      <div class="ps-row ps-gutters">
        <div class="ps-col-lg-5">
          <div class="ps-contacts__title"><?php echo get_post_meta($post->ID, 'contact_home_form_pretitle', true); ?></div>
          <?php
          $cfID = get_post_meta($post->ID, 'contact_home_form_id', true);
          $cf = '[contact-form-7 id="' . $cfID . '" html_class="ps-form" title="Contact form"]';
          ?>
          <div class="ps-form__wrap">
            <?php echo do_shortcode($cf); ?>
          </div>
        </div>
        <?php $socials = get_option('options_socials_list');
        if ($socials) : ?>
          <div class="ps-col-lg-5 ps-offset-lg-2">
            <div class="ps-contacts__title"><?php echo get_option('options_socials_title'); ?></div>
            <ul class="ps-social__list">
              <?php
              for ($i = 0; $i < $socials; $i++) : ?>
                <li class="ps-social__item" style="background-image: url('<?php echo wp_get_attachment_image_url(get_option('options_socials_list_' . $i . '_icon'), 'full'); ?>');">
                  <span class="ps-social__name"><?php echo get_option('options_socials_list_' . $i . '_label'); ?></span>
                  <?php $itemType = get_option('options_socials_list_' . $i . '_type');
                  if ($itemType == 'email') : ?>
                    <a class="ps-social__link" href="mailto:<?php echo get_option('options_socials_list_' . $i . '_url'); ?>">
                      <?php echo get_option('options_socials_list_' . $i . '_link_text'); ?>
                    </a>
                  <?php else : ?>
                    <a class="ps-social__link" href="<?php echo get_option('options_socials_list_' . $i . '_url'); ?>" target="_blank">
                      <?php echo get_option('options_socials_list_' . $i . '_link_text'); ?>
                    </a>
                  <?php endif; ?>
                </li>
              <?php endfor; ?>
            </ul>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>