<?php 
$readings_items = get_cached_posts(array(
  'post_type'    => 'readings',
  'posts_per_page' => 4,
  'post_status'  => 'publish'), 
3600, 'readings');
if ($readings_items): 
  $readingsId = get_option('options_recommended_readings_page'); ?>
  <section class="ps-section">
    <div class="ps-container">
      <div class="ps-row ps-gutters">
        <div class="ps-col-md-4 ps-col-md-4">
          <h2 class="is-t-uppercase"><?php echo get_the_title($readingsId); ?></h2>
        </div>
      </div>
      <div class="ps-section__row">
        <div class="ps-item__row">
          <div class="ps-row ps-gutters">
            <?php foreach ($readings_items as $reading): ?>
              <div class="ps-col-md-6 ps-col-lg-3">
                <div class="ps-item">
                  <?php
                  $thumb_id = get_post_thumbnail_id($reading);
                  $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                  ?>
                  <div class="ps-item__img">
                    <img src="<?php echo get_the_post_thumbnail_url($reading, 'medium'); ?>" alt="<?php echo $alt ? $alt :$reading->post_title ?>">
                  </div>
                  <ul class="ps-hierarchy__list">
                    <li class="ps-hierarchy__list--item is-fw-bold"><?php echo $reading->post_title ?></li>
                    <li class="ps-hierarchy__list--item">
                      <?php echo get_post_meta($reading->ID, 'reading_author', true); ?>
                      <?php if ($readingYear = get_post_meta($reading->ID, 'reading_year', true)): ?>
                        <span> <?php echo $readingYear; ?>
                      <?php endif; ?>
                    </li>
                    <li class="ps-hierarchy__list--item is-color-t-secondary"><?php echo get_post_meta($reading->ID, 'reading_publisher', true); ?></li>
                  </ul>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
      <div class="ps-btn__wrap_center">
        <a href="<?php echo esc_url(get_page_link($readingsId)); ?>" class="ps-btn"><?php echo get_option('options_read_more_button'); ?></a>
      </div>
    </div>
  </section>
<?php endif; ?>
