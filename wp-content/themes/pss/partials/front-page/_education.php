<?php
$educationList = get_post_meta($post->ID, 'education_list', true); 
if ($educationList): ?>
  <section class="ps-section">
    <div class="ps-container">
      <div class="ps-row ps-gutters">
        <div class="ps-col-lg-4">
          <h2 class="is-t-uppercase"><?php echo get_post_meta($post->ID, 'education_title', true); ?></h2>
        </div>
        <div class="ps-col-lg-5 ps-offset-lg-2">
          <div class="ps-hierarchy ps-hierarchy_marked">
            <?php
            for ($i = 0; $i < $educationList; $i++): ?>
              <div class="ps-hierarchy__item">
                <ul class="ps-hierarchy__list">
                  <li class="ps-hierarchy__marker"></li>
                  <li class="ps-hierarchy__list--item h5 is-fw-medium"><?php echo get_post_meta($post->ID, 'education_list_'.$i.'_position', true); ?></li>
                  <li class="ps-hierarchy__list--item h5"><?php echo get_post_meta($post->ID, 'education_list_'.$i.'_place', true); ?></li>
                  <li class="ps-hierarchy__list--item ps-text_md is-color-t-secondary"><?php echo get_post_meta($post->ID, 'education_list_'.$i.'_department', true); ?></li>
                  <li class="ps-hierarchy__list--item ps-text_md"><?php echo get_post_meta($post->ID, 'education_list_'.$i.'_time', true); ?></li>
                </ul>
              </div>
            <?php endfor; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>