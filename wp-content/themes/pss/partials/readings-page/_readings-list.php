<?php 
$readings_items = get_cached_posts(array(
  'post_type'    => 'readings',
  'posts_per_page' => -1,
  'post_status'  => 'publish'), 
3600, 'readings');
if ($readings_items): 
  $readingsId = get_option('options_recommended_readings_page'); ?>
  <section>
    <div class="ps-container">
      <h1 class="h2 is-t-uppercase"><?php echo get_the_title($readingsId); ?></h1>
      <div class="ps-section__row">
        <div class="ps-row ps-gutters">
          <div class="ps-col-md-10 ps-offset-md-1">
            <?php foreach ($readings_items as $reading): ?>
              <div class="ps-item ps-item_rowdir">
                <?php
                $thumb_id = get_post_thumbnail_id($reading);
                $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
                ?>
                <div class="ps-item__img">
                  <img src="<?php echo get_the_post_thumbnail_url($reading, 'medium'); ?>" alt="<?php echo $alt ? $alt :$reading->post_title ?>">
                </div>
                <ul class="ps-hierarchy__list">
                  <li class="ps-hierarchy__list--item h4"><?php echo $reading->post_title ?></li>
                  <li class="ps-hierarchy__list--item h5 is-fw-medium">
                    <?php echo get_post_meta($reading->ID, 'reading_author', true); ?>
                    <?php if ($readingYear = get_post_meta($reading->ID, 'reading_year', true)): ?>
                      <span> <?php echo $readingYear; ?>
                    <?php endif; ?>
                  </li>
                  <li class="ps-hierarchy__list--item ps-text_md is-color-t-secondary"><?php echo get_post_meta($reading->ID, 'reading_publisher', true); ?></li>
                  <li class="ps-hierarchy__list--item ps-text_md"><?php echo get_post_meta($reading->ID, 'reading_notice', true); ?></li>
                </ul>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
