<?php
$args = array(
  'post_type' => 'articles',
  'posts_per_page' => 9,
  'orderby'	=> 'post_date',
  'order'         => 'DESC'
);
$query = new WP_Query($args); 
if ($query->have_posts()): 
  $blogId = get_option('options_blog_page'); ?>
  <section>
    <div class="ps-container">
      <h1 class="is-t-uppercase"><?php echo get_the_title($blogId); ?></h1>
      <div class="ps-section__row">
        <div class="ps-article__list">
          <div class="ps-row ps-gutters" data-posts-list>
            <?php while ($query->have_posts()) : $query->the_post();
              get_template_part('partials/blog-page/_blog-thumb');
            endwhile; 
            ?>
          </div>
        </div>
      </div>
      <?php 
      if ($query->max_num_pages > 1): ?>
        <div class="ps-btn__wrap_center">
          <div class="ps-btn ps-btn_more" data-load-more>
            <?php echo get_option('options_load_more_button'); ?>
          </div>
        </div>
      <?php endif; 
      wp_reset_postdata(); ?>
    </div>
  </section>
<?php endif; ?>