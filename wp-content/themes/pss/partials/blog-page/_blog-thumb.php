<div class="ps-col-md-6 ps-col-lg-4 ps-article">
  <a href="<?php echo get_permalink($post->ID) ?>" class="ps-article__thumb">
    <?php
    $thumb_id = get_post_thumbnail_id($post->ID);
    $alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
    ?>
    <div class="ps-article__img">
      <img src="<?php echo get_the_post_thumbnail_url($article, 'medium'); ?>" alt="<?php echo $alt ? $alt :$article->post_title ?>">
    </div>
    <div class="ps-article__inner">
      <h3 class="ps-article__title"><?php echo the_title(); ?></h3>
      <p class="ps-article__precopy"><?php echo substr(strip_tags($post->post_content), 0, 250) ?></p>
    </div>
  </a>
</div>