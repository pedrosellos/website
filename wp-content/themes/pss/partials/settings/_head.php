<meta charset="utf-8">
<meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php wp_title(); ?></title>

<!-- Favicon -->


<meta name="format-detection" content="telephone=no">
<meta name="author" content="Lionwood Software">

<link rel="preload" as="font" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/Avenir-Black.woff2" type="font/woff2" crossorigin="anonymous">
<link rel="preload" as="font" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/Avenir-Medium.woff2" type="font/woff2" crossorigin="anonymous">
<link rel="preload" as="font" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/Avenir-Roman.woff2" type="font/woff2" crossorigin="anonymous">
