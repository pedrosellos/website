<?php

/* Template Name: Contact Page */

get_header(); ?>

<main class="ps-page">
  <section>
    <div class="ps-container">
      <div class="ps-row ps-gutters">
        <div class="ps-col-md-10 ps-offset-md-1">
          <h1 class="h2 is-t-uppercase"><?php echo get_post_meta($post->ID, 'contact_page_title', true); ?></h1>
          <div class="h5 is-color-t-secondary"><?php echo get_post_meta($post->ID, 'contact_page_description', true); ?></div>
          <div class="ps-form__section">
            <div class="ps-row ps-gutters">
              <div class="ps-col-md-10 ps-col-lg-7">
                <h2 class="h4"><?php echo get_post_meta($post->ID, 'contact_form_title', true); ?></h2>
                <div class="ps-text_md is-color-t-secondary"><?php echo get_post_meta($post->ID, 'contact_form_description', true); ?></div>
                <?php
                $cfID = get_post_meta($post->ID, 'contact_form_id', true);
                $cf = '[contact-form-7 id="' . $cfID . '" html_class="ps-form" title="Contact form"]';
                ?>
                <div class="ps-form__wrap">
                  <?php echo do_shortcode($cf); ?>
                </div>
              </div>
            </div>
          </div>
          <?php $socials = get_option('options_socials_list');
          if ($socials) : ?>
            <div class="ps-form__section">
              <h2 class="h4"><?php echo get_option('options_socials_title'); ?></h2>
              <div class="ps-text_md is-color-t-secondary"><?php echo get_option('options_socials_description'); ?></div>
              <ul class="ps-social__list ps-social__list_row">
                <?php
                for ($i = 0; $i < $socials; $i++) : ?>
                  <li class="ps-social__item" style="background-image: url('<?php echo wp_get_attachment_image_url(get_option('options_socials_list_' . $i . '_icon'), 'full'); ?>');">
                    <span class="ps-social__name"><?php echo get_option('options_socials_list_' . $i . '_label'); ?></span>
                    <?php $itemType = get_option('options_socials_list_' . $i . '_type');
                    if ($itemType == 'email') : ?>
                      <a class="ps-social__link" href="mailto:<?php echo get_option('options_socials_list_' . $i . '_url'); ?>">
                        <?php echo get_option('options_socials_list_' . $i . '_link_text'); ?>
                      </a>
                    <?php else : ?>
                      <a class="ps-social__link" href="<?php echo get_option('options_socials_list_' . $i . '_url'); ?>" target="_blank">
                        <?php echo get_option('options_socials_list_' . $i . '_link_text'); ?>
                      </a>
                    <?php endif; ?>
                  </li>
                <?php endfor; ?>
              </ul>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>