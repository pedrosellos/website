<?php 

get_header(); ?>

<main class="ps-page">
  <section>
    <div class="ps-container">
      <?php 
      if (get_option('options_back_to_blog_button')):
      $blogId = get_option('options_blog_page') ?>
        <a href="<?php echo esc_url(get_page_link($blogId)); ?>" class="ps-btn ps-btn_back" data-load-more>
          <?php echo get_option('options_back_to_blog_button'); ?>
        </a>
      <?php endif; ?>
      <h1><?php echo the_title(); ?></h1>
      <div class="ps-row ps-gutters">
        <div class="ps-col-lg-8 ps-offset-lg-2">
          <div class="ps-post">
            <?php echo the_content(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>