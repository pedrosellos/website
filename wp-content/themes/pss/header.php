<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <?php get_template_part('partials/settings/_head'); ?>
  <?php wp_head(); ?>
</head>

<body>
  <header class="ps-header" data-header>
    <?php if (has_nav_menu('primary')) : ?>
      <nav class="ps-nav">
        <?php
        wp_nav_menu(
          array(
            'theme_location' => 'primary',
            'menu_class' => false,
            'container_class' => false,
            'container' => false,
            'container_id' => false,
            'menu_id' => false,
            'depth' => 0,
            'fallback_cb' => '',
            'items_wrap' => '<ul class="ps-nav__menu">%3$s</ul>'
          )
        );
        ?>
      </nav>
      <div class="ps-burger" data-burger-btn>
        <span class="ps-burger__line"></span>
        <span class="ps-burger__line"></span>
        <span class="ps-burger__line"></span>
      </div>
    <?php endif; ?>
  </header>